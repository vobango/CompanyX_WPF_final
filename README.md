## Changelog (tegevuste ajalugu)

#### Kuvab kasutajate poolt tehtud muudatused (kes muutis kelle andmeid) ajalises j�rjekorras, uuemad eespool.

Kuvatav logi on Listbox t��pi element, mis t�idetakse LogItem klassi kuuluvate objektidega. 
Klassi LogItem objektid luuakse esmakordselt p�hiakna (MainUI v�i WorkerUI) konstruktori v�ljakutsumisel. 
Elemendid koosnevad kahest nimest (muutja ja muudetav) ning muudatuse tegemise ajast (datetime t��pi muutuja). 
K�ik andmed objektide loomiseks p�rinevad andmebaasist, ChangeLog tabeli vastavatest veergudest (muutja -> ModifierId, moodetav -> EmployeeId, aeg -> ChangeTime).


Logiakna refresh nupuga on v�imalik kuvada uusi muudatusi, mis sessiooni jooksul andmebaasi on tehtud (eeldusel, et need on ChangeLog tabelisse sisse kantud).  
Nupule vajutamine loob k�ik LogItem objektid uuesti.



## Andmebaasi mudel

Andmebaas koosneb viiest tabelist:
* Category (Oskused)
* ChangeLog (Muudatuste logi)
* Course (Kursused)
* Employee (T��tajad)
* Skill (Iga t��taja oskused)

Tabel Category on seotud tabeliga Skill, et oleks v�imalik grupeerida t��tajaid oskuste kaupa (nt. oskus Y on t��tajatel 1, 2 ja 3).  
ChangeLog tabel koosneb kahest isikukoodist ja muudatuse tegemise ajast. Isikukoodid vastavad t��tajate tabelis sisalduvatele koodidele (IdCode).   
Course tabelis sisalduvad kursused, mis koosnevad isikust, kes kursusel osaleb (isikukood, seotud Employee tabeli IdCode reaga), kursuse nimest, t��bist, lisainfost ja boolean v��rtusest, kas kursus on l�bitud v�i tulekul (vastavalt True v�i False). *Olemas on ka veerg Data, milles peaks sisalduma dokumendifail vastava kursuse l�bimise kohta, kuid hetkel puudub sel funktsionaalsus.*   
Tabel Employee sisaldab t��tajaid, kellel on isikukood (IdCode), t�isnimi, andmebaasi lisamise aeg ja boolean v��rtused, mis t�histavad t��taja olekut (veerg Active, aktiivne - True v�i mitteaktiivne - False) ning staatust (veerg Rights, tavakasutaja - False, admin - True).   
Skill tabel koosneb �ksikutest oskustest, t��taja kaupa. Iga oskus on seotud isikuga (veerg IdCode), oskusel on nimi (veerg SkillName, seotud tabeliga Category) ja tase, mis vastaval isikul vastavas oskuses on (veerg Mastery, v��rtused 1-4).


