﻿#pragma checksum "..\..\WorkerUI.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "3955E32C44E6D499DAB5626896CC2B12"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CompanyX_WPF;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace CompanyX_WPF {
    
    
    /// <summary>
    /// WorkerUI
    /// </summary>
    public partial class WorkerUI : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CompanyX_WPF.WorkerUI WorkerWindow;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Menu MainMenu;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem LogOut;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem Quit;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem SearchName;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem SearchSkill;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock GreetingLabel;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel EventsTable;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel EventsHeader;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid EventTable;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RefreshBtn;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\WorkerUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox LogList;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/CompanyX_WPF;component/workerui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\WorkerUI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.WorkerWindow = ((CompanyX_WPF.WorkerUI)(target));
            return;
            case 2:
            this.MainMenu = ((System.Windows.Controls.Menu)(target));
            return;
            case 3:
            this.LogOut = ((System.Windows.Controls.MenuItem)(target));
            
            #line 14 "..\..\WorkerUI.xaml"
            this.LogOut.Click += new System.Windows.RoutedEventHandler(this.LogOut_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Quit = ((System.Windows.Controls.MenuItem)(target));
            
            #line 21 "..\..\WorkerUI.xaml"
            this.Quit.Click += new System.Windows.RoutedEventHandler(this.Quit_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 30 "..\..\WorkerUI.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.MenuItem_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.SearchName = ((System.Windows.Controls.MenuItem)(target));
            
            #line 39 "..\..\WorkerUI.xaml"
            this.SearchName.Click += new System.Windows.RoutedEventHandler(this.SearchName_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.SearchSkill = ((System.Windows.Controls.MenuItem)(target));
            
            #line 40 "..\..\WorkerUI.xaml"
            this.SearchSkill.Click += new System.Windows.RoutedEventHandler(this.SearchSkill_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.GreetingLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.EventsTable = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 10:
            this.EventsHeader = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.EventTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 12:
            this.RefreshBtn = ((System.Windows.Controls.Button)(target));
            
            #line 141 "..\..\WorkerUI.xaml"
            this.RefreshBtn.Click += new System.Windows.RoutedEventHandler(this.RefreshBtn_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.LogList = ((System.Windows.Controls.ListBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

