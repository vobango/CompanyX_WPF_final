﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for SearchByName.xaml
    /// </summary>
    public partial class SearchByName : Window
    {
        CompanyXEntities CompX = new CompanyXEntities();

        public SearchByName()
        {
            InitializeComponent();
            SearchCombo.ItemsSource = CompX.Employees.Select(x => x.FullName).ToList();
        }

        public void SearchCombo_Key(object sender, KeyEventArgs e)
        {
            var person = CompX.Employees.Where(x => x.FullName == SearchCombo.Text).FirstOrDefault();

            string skill = "";
            CompX.Skills
               .Where(x => x.IdCode == person.IdCode)
               .ToList()
            .ForEach(x => skill += (x.SkillName + ", level " + x.Mastery + "\n"));

            MessageBox.Show($"{person.FullName} skills: \n\n{skill}", "Results");
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            var person = CompX.Employees.Where(x => x.FullName == SearchCombo.Text).FirstOrDefault();

            string skill = "";
            CompX.Skills
               .Where(x => x.IdCode == person.IdCode)
               .OrderByDescending(x => x.Mastery)
               .ToList()
            .ForEach(x => skill += (x.SkillName + ", level " + x.Mastery + "\n"));

            MessageBox.Show($"{person.FullName} skills: \n\n{skill}", "Employee");
        }
    }
}
