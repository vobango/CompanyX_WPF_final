﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for WorkerUI.xaml
    /// </summary>
    public partial class WorkerUI : Window
    {
        CompanyXEntities CompX = new CompanyXEntities();
        public string UserId;
        public class LogItem
        {
            public string Employee { get; set; }
            public string Modifier { get; set; }
            public DateTime ChangeTime { get; set; }
            public override string ToString()
            {
                return $"{Modifier} changed details for {Employee} at {ChangeTime}.";
            }
        }
        public WorkerUI()
        {
            InitializeComponent();
        }

        public WorkerUI(string id)
        {
            InitializeComponent();
            UserId = id;
            var Logs = CompX.ChangeLogs.ToList();
            List<LogItem> logs = new List<LogItem>();
            foreach (var log in Logs)
            {
                logs.Add(new LogItem
                {
                    Employee = CompX.Employees.Where(x => x.IdCode == log.EmployeeId).ToList().First().FullName,
                    Modifier = CompX.Employees.Where(x => x.IdCode == log.ModifierId).ToList().First().FullName,
                    ChangeTime = log.ChangeTime
                });
            }
            LogList.ItemsSource = logs.OrderByDescending(x => x.ChangeTime);
            EventTable.ItemsSource = CompX.Courses.Where(x => x.IdCode == id).ToList().Take(CompX.Courses.Where(x => x.IdCode == id).ToList().Count).OrderBy(x => x.Passed);
            GreetingLabel.Text = "Welcome, " + CompX.Employees.Where(x => x.IdCode == UserId).First().FullName.ToString() + "!";
            //EventList.ItemsSource = CompX.Courses.Where(x => x.IdCode == id).ToList();

            if (CompX.Courses.Where(x => x.IdCode == id).ToList().Count > 0)
            {
                MessageBox.Show($"You have {CompX.Courses.Where(x => x.IdCode == id && x.Passed == false).ToList().Count} upcoming course(s).","Upcoming Events",MessageBoxButton.OK,MessageBoxImage.Exclamation);
            }
        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow newLogin = new LoginWindow();
            MessageBoxResult result = MessageBox.Show("Are you sure?", "Log Out", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes);
            if (result == MessageBoxResult.Yes)
            {
                newLogin.Show();
                Close();
            }
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Exit the Program?", "Quit", MessageBoxButton.OKCancel, MessageBoxImage.Warning, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                Close();
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddCourseWindow newEvent = new AddCourseWindow(UserId);
            newEvent.Show();
        }

        private void RefreshBtn_Click(object sender, RoutedEventArgs e)
        {
            var Logs = CompX.ChangeLogs.ToList();
            List<LogItem> logs = new List<LogItem>();
            foreach (var log in Logs)
            {
                logs.Add(new LogItem
                {
                    Employee = CompX.Employees.Where(x => x.IdCode == log.EmployeeId).ToList().First().FullName,
                    Modifier = CompX.Employees.Where(x => x.IdCode == log.ModifierId).ToList().First().FullName,
                    ChangeTime = log.ChangeTime
                });
            }
            LogList.ItemsSource = logs.OrderByDescending(x => x.ChangeTime);
            LogList.Items.Refresh();
        }

        private void ChangeEvent_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            AddCourseWindow newEvent = new AddCourseWindow(UserId);
            newEvent.Show();
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //ChangeEvent.IsEnabled = true;
        }

        private void SearchName_Click(object sender, RoutedEventArgs e)
        {
            SearchByName search = new SearchByName();
            search.Show();
        }

        private void SearchSkill_Click(object sender, RoutedEventArgs e)
        {
            SearchBySkill search = new SearchBySkill();
            search.Show();
        }
    }
}
