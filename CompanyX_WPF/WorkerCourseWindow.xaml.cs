﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for WorkerCourseWindow.xaml
    /// </summary>

    public partial class WorkerCourseWindow : Window
    {
        CompanyXEntities CompX = new CompanyXEntities();
        public string UserId;
        public WorkerCourseWindow()
        {
            InitializeComponent();
        }

        public WorkerCourseWindow(string id)
        {
            InitializeComponent();
            UserId = id;
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {               
                var courseType = "";
                bool passed = false;
                if (CourseBox.IsChecked == true)
                {
                    courseType = "Course";
                }
                else
                {
                    courseType = "Exam";
                }
                if (PassedBox.IsChecked == true)
                {
                    passed = true;
                }

                MessageBoxResult result = System.Windows.MessageBox.Show($"Add following data to Course table?\n\n" +
                    $"IdCode - {UserId}\n" +
                    $"CourseName - {CEname.Text}\n" +
                    $"CourseType - {courseType}\n" +
                    $"Passed - {passed.ToString()}", "Confirm Event", MessageBoxButton.YesNo, MessageBoxImage.Asterisk, MessageBoxResult.Yes);
                if (result == MessageBoxResult.Yes)
                {
                    CompX.Courses.Add(new Course
                    {
                        IdCode = UserId,
                        CourseName = CEname.Text,
                        CourseType = courseType,
                        Textfield = InfoBox.Text,
                        Passed = passed
                    });
                    CompX.ChangeLogs.Add(new ChangeLog
                    {
                        EmployeeId = UserId,
                        ModifierId = UserId,
                        ChangeTime = DateTime.Now
                    });
                    CompX.SaveChanges();
                    Close();
                }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BrowseImage_Click(object sender, RoutedEventArgs e)
        {
            Loadimage();
        }

        private void Loadimage()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.DefaultExt = "jpeg";
            fileDialog.Filter = "Image files (*.jpeg)|*.jpeg|JPG files|*.jpg|All files (*.*)|*.*";
            fileDialog.ShowDialog();
        }

        private void PassedBox_Checked(object sender, RoutedEventArgs e)
        {
            BrowseImage.IsEnabled = true;
        }

        private void PassedBox_Unchecked(object sender, RoutedEventArgs e)
        {
            BrowseImage.IsEnabled = false;
        }
    }
}
