﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for SearchBySkill.xaml
    /// </summary>
    public partial class SearchBySkill : Window
    {
        CompanyXEntities CompX = new CompanyXEntities();
        public SearchBySkill()
        {
            InitializeComponent();
            SearchCombo.ItemsSource = CompX.Skills.Select(x => x.SkillName).Distinct().ToList();
        }

        private void SearchCombo_KeyDown(object sender, KeyEventArgs e)
        {
            var skill = SearchCombo.Text;
            string people = "";
            CompX.Skills
                .Where(x => x.SkillName == skill)
                .OrderByDescending(x => x.Mastery)
                .ToList()
                .ForEach(x => people += (x.Employee.FullName + ", level " + x.Mastery + "\n"));
            MessageBox.Show($"Employees with {skill} skill:\n\n{people}", "Results");
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            var skill = SearchCombo.Text;
            string people = "";
            CompX.Skills
                .Where(x => x.SkillName == skill)
                .OrderByDescending(x => x.Mastery)
                .ToList()
                .ForEach(x => people += (x.Employee.FullName + ", level " + x.Mastery + "\n"));
            MessageBox.Show($"Employees with {skill} skill:\n\n{people}", "Results");
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
