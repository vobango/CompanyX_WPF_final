//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CompanyX_WPF
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChangeLog
    {
        public int Qnr { get; set; }
        public string EmployeeId { get; set; }
        public string ModifierId { get; set; }
        public System.DateTime ChangeTime { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual Employee Employee1 { get; set; }
    }
}
