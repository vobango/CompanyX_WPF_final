﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for MainUI.xaml
    /// </summary>
    public partial class MainUI : Window
    {
        CompanyXEntities CompX = new CompanyXEntities();
        public string UserId;
        public class LogItem
        {
            public string Employee { get; set; }
            public string Modifier { get; set; }
            public DateTime ChangeTime { get; set; }
            public override string ToString()
            {
                return $"{Modifier} changed details for {Employee} at {ChangeTime}."; 
            }
        }
        public MainUI()
        {
            InitializeComponent();
            var Logs = CompX.ChangeLogs.ToList();
            List<LogItem> logs = new List<LogItem>();
            foreach (var log in Logs)
            {
                logs.Add(new LogItem
                {
                    Employee = CompX.Employees.Where(x => x.IdCode == log.EmployeeId).ToList().First().FullName,
                    Modifier = CompX.Employees.Where(x => x.IdCode == log.ModifierId).ToList().First().FullName,
                    ChangeTime = log.ChangeTime
                });
            }
            LogList.ItemsSource = logs.OrderByDescending(x => x.ChangeTime);
        }
        public MainUI(string id)
        {
            InitializeComponent();
            UserId = id;
            var Logs = CompX.ChangeLogs.ToList();
            List<LogItem> logs = new List<LogItem>();
            foreach (var log in Logs)
            {
                logs.Add(new LogItem
                {
                    Employee = CompX.Employees.Where(x => x.IdCode == log.EmployeeId).ToList().First().FullName,
                    Modifier = CompX.Employees.Where(x => x.IdCode == log.ModifierId).ToList().First().FullName,
                    ChangeTime = log.ChangeTime
                });
            }
            InfoTable.ItemsSource = CompX.Employees.ToList().Take(CompX.Employees.ToList().Count).OrderBy(x => x.FullName);
            EventTable.ItemsSource = CompX.Courses.ToList().Take(CompX.Courses.ToList().Count).OrderBy(x => x.CourseName);
            SkillT.ItemsSource = CompX.Categories.ToList();
            LogList.ItemsSource = logs.OrderByDescending(x => x.ChangeTime);
            GreetingLabel.Text = "Welcome, " + CompX.Employees.Where(x => x.IdCode == UserId).First().FullName.ToString() + "!";
            //EventList.ItemsSource = CompX.Courses.ToList();
            //CourseGrid.ItemsSource = CompX.Courses.ToList();

            SearchBox.Text = "Search";
        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow newLogin = new LoginWindow();
            MessageBoxResult result = MessageBox.Show("Are you sure?", "Log Out", MessageBoxButton.YesNo, MessageBoxImage.Question,MessageBoxResult.Yes);
            if (result == MessageBoxResult.Yes)
            {
                newLogin.Show();
                Close();
            }
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Exit the Program?", "Quit", MessageBoxButton.OKCancel, MessageBoxImage.Warning, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                Close();
            }
        }

        private void RefreshBtn_Click(object sender, RoutedEventArgs e)
        {
            var Logs = CompX.ChangeLogs.ToList();
            List<LogItem> logs = new List<LogItem>();
            foreach (var log in Logs)
            {
                logs.Add(new LogItem
                {
                    Employee = CompX.Employees.Where(x => x.IdCode == log.EmployeeId).ToList().First().FullName,
                    Modifier = CompX.Employees.Where(x => x.IdCode == log.ModifierId).ToList().First().FullName,
                    ChangeTime = log.ChangeTime
                });
            }
            LogList.ItemsSource = logs.OrderByDescending(x => x.ChangeTime);
            LogList.Items.Refresh();
        }

        //private void ChangeEvent_Click(object sender, RoutedEventArgs e)
        //{

        //}

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            AddCourseWindow newEvent = new AddCourseWindow(UserId);
            newEvent.Show();
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //ChangeEvent.IsEnabled = true;
        }

        private void SaveChanges_Click(object sender, RoutedEventArgs e)
        {
            CompX.SaveChanges();
        }

        private void AddWorker_Click(object sender, RoutedEventArgs e)
        {
            AddWorkerWindow addWorker = new AddWorkerWindow(UserId);
            addWorker.Show();
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            InfoTable.ItemsSource = CompX.Employees.Where(x => x.FullName.Contains(SearchBox.Text) || x.IdCode.Contains(SearchBox.Text)).ToList()
                .Take(CompX.Employees.Where(x => x.FullName.Contains(SearchBox.Text) || x.IdCode.Contains(SearchBox.Text)).ToList().Count);
        }

        private void SearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            SearchBox.Text = "";
        }

        private void SearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            SearchBox.Text = "Search";
        }

        private void NewEvent_Click(object sender, RoutedEventArgs e)
        {
            AddCourseWindow newEvent = new AddCourseWindow(UserId);
            newEvent.Show();
        }

        private void AddWorkerMenu_Click(object sender, RoutedEventArgs e)
        {
            AddWorkerWindow addWorker = new AddWorkerWindow(UserId);
            addWorker.Show();
        }

        private void NewEventMenu_Click(object sender, RoutedEventArgs e)
        {
            AddCourseWindow newEvent = new AddCourseWindow(UserId);
            newEvent.Show();
        }

        private void SearchName_Click(object sender, RoutedEventArgs e)
        {
            SearchByName search = new SearchByName();
            search.Show();
        }

        private void SearchSkill_Click(object sender, RoutedEventArgs e)
        {
            SearchBySkill search = new SearchBySkill();
            search.Show();
        }
    }
}
