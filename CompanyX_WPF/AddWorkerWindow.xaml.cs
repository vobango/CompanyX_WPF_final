﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for AddWorkerWindow.xaml
    /// </summary>
    public partial class AddWorkerWindow : Window
    {
        CompanyXEntities compX = new CompanyXEntities();
        public string UserId;
        public AddWorkerWindow()
        {
            InitializeComponent();

            SkillBox.ItemsSource = compX.Categories.Select(x => x.CategoryName).ToList();

        }

        public AddWorkerWindow(string id)
        {
            InitializeComponent();
            UserId = id;
            SkillBox.ItemsSource = compX.Categories.Select(x => x.CategoryName).ToList();
        }


        private void IdBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (((TextBox)sender).Text.Length < 11)
            {
                MessageBox.Show("Id code must have 11 numbers");
                IdBox.Clear();
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            var skillComboBox = new ComboBox();
            skilltable.Children.Add(skillComboBox);

            var levelComboBox = new ComboBox();
            leveltable.Children.Add(levelComboBox);
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (IdBox.ToString() == "" || NameBox.ToString() == "")
            {
                MessageBox.Show("Please fill all fields");
            }
            else
            {
                // salvestab ainult 1 skill ja level korraga
                //kui on lisatud 2 siis viskab errori
                //vaja koondada
                var skill = compX.Skills.Where(x => x.SkillName == SkillBox.SelectionBoxItem.ToString()).ToList();

                var level = compX.Skills.Where(x => x.Mastery == LevelBox.SelectionBoxItem.ToString()).ToList();


                bool active = false;
                bool admin = false;
                if (ActiveBox.IsChecked == true)
                {
                    active = true;
                }
                if (AdminBox.IsChecked == true)
                {
                    admin = true;
                }

                compX.Employees.Add(new Employee
                {
                    IdCode = IdBox.Text,
                    FullName = NameBox.Text,
                    Active = active,
                    Rights = admin,
                    Adddate = DateTime.Today
                });

                compX.Skills.Add(new Skill
                {
                    IdCode = IdBox.Text,
                    SkillName = skill.First().SkillName,
                    Mastery = level.First().Mastery,

                });

                compX.ChangeLogs.Add(new ChangeLog
                {
                    EmployeeId = IdBox.Text,
                    ModifierId = UserId,
                    ChangeTime = DateTime.Now
                });
                compX.SaveChanges();
                Close();
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

    }
}
