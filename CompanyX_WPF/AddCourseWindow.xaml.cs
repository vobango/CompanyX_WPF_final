﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for AddCourseWindow.xaml
    /// </summary>
    public partial class AddCourseWindow : Window
    {
        CompanyXEntities CompX = new CompanyXEntities();
        public string UserId;
        public class FileName
        {
            public string PathName { get; set; }
        }
        public AddCourseWindow()
        {
            InitializeComponent();
            WorkerBox.ItemsSource = CompX.Employees.Select(x => x.FullName).ToList();
        }
        public AddCourseWindow(string id)
        {
            InitializeComponent();
            UserId = id;
            WorkerBox.ItemsSource = CompX.Employees.Select(x => x.FullName).ToList();
        }
        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (WorkerBox.SelectionBoxItem.ToString() == "")
            {
               System.Windows.MessageBox.Show("Please select an employee!","Error");
            }
            else
            {            
                var employee = CompX.Employees.Where(x => x.FullName == WorkerBox.SelectionBoxItem.ToString()).ToList();
                var courseType = "";
                bool passed = false;
                if (CourseBox.IsChecked == true)
                {
                    courseType = "Course";                    
                }
                else
                {
                    courseType = "Exam";
                }
                if (PassedBox.IsChecked == true)
                {
                    passed = true;
                }

                MessageBoxResult result = System.Windows.MessageBox.Show($"Add following data to Course table?\n\n" +
                    $"IdCode - {employee.First().IdCode}\n" +
                    $"CourseName - {CEname.Text}\n" +
                    $"CourseType - {courseType}\n" +
                    $"Passed - {passed.ToString()}","Confirm Event", MessageBoxButton.YesNo,MessageBoxImage.Asterisk,MessageBoxResult.Yes);
                if (result == MessageBoxResult.Yes)
                {
                    CompX.Courses.Add(new Course
                    {
                        IdCode = employee.First().IdCode,
                        CourseName = CEname.Text,
                        CourseType = courseType,
                        Textfield = InfoBox.Text,
                        Passed = passed,
                        Data = null
                    });
                    CompX.ChangeLogs.Add(new ChangeLog
                    {
                        EmployeeId = employee.First().IdCode,
                        ModifierId = UserId,
                        ChangeTime = DateTime.Now
                    });
                    CompX.SaveChanges();
                    Close();
                }                
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BrowseImage_Click(object sender, RoutedEventArgs e)
        {            
            Loadimage();
        }

        private void Loadimage()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.DefaultExt = "jpeg";
            fileDialog.Filter = "Image files (*.jpeg)|*.jpeg|JPG files|*.jpg|All files (*.*)|*.*";
            fileDialog.ShowDialog();            
        }

        private void PassedBox_Checked(object sender, RoutedEventArgs e)
        {
            BrowseImage.IsEnabled = true;
        }

        private void PassedBox_Unchecked(object sender, RoutedEventArgs e)
        {
            BrowseImage.IsEnabled = false;
        }
    }
}
